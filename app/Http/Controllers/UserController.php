<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserInfo;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $user = null;

    public function __construct(User $user)
    {
        $this->user = $user;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validateId($id);
        $rules = $this->user->updateUserRules();
        $request->validate($rules);

        $data = $request->except('image', '_method', '_token');


        if($request->image){
            $image_name = uploadImage($request->image, "users", "300x300");
            if($image_name){
                $data['image'] = $image_name;
                if($this->user->userInfo != null){
                    deleteImage($this->user->userInfo->image, "user");
                }
            }
        }
        $this->user->fill($data);
        $success = $this->user->save();
        if($success){
            //update user
            $data['user_id'] = $this->user->id;

            if($this->user->userInfo == null){
                $this->user->userInfo = new UserInfo();
            }
            $this->user->userInfo->fill($data);
            $this->user->userInfo->save();

            $request->session()->flash('success', 'You are .');
        }else{
            $request->session()->flash('error', 'Sorry, there was a problem while updating user.');
        }
        return redirect()->back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function updateUser(Request $request)
    {
        dd($request);
    }

    private function validateId($id){
        $this->user = $this->user->find($id);
        if(!$this->user){
            request()->session()->flash('error', 'User Not Found.');
            return redirect()->back();
        }
    }

    public function updatePassword(Request $request,  $id)
    {
        $this->validateId($id);

        $request->validate($this->user->passwordUpdateRule());

        $this->user->password = bcrypt($request->password);
        $success = $this->user->save();

        if($success){
            if($request->user()->id == $this->user->id){
                // current logged in user updated password
                auth()->logout();
                return redirect()->route('login');
            }else{
                $request->session()->flash('success', 'Password, change successfully.');
                return redirect()->route("home");
            }
        }else{
            $request->session()->flash('error', 'Sorry! Password could not be updated at this moment.');
            return redirect()->back();
        }

    }
}
