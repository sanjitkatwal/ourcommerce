<?php
     function uploadImage($image, $dir, $thumb=null){
         $dir = strtolower($dir);
        $path = public_path().'/uploads/'.$dir;

        if(!File::exists($path)){
            File::makeDirectory($path, 0777, true, true); //path, permission, recursive call, closer call
        }

        // User-202012061
        $file_name = ucfirst($dir)."-".date("YmdHis").rand(0,999999).".".$image->getClientOriginalExtension();

        $status = $image->move($path, $file_name);
        if($status){
            // after file uploaded, 200x200, 200x400
            if($thumb !== null){
                list($width, $height) = explode("x", $thumb);
                Image::make($path.".".$file_name)->resize($width,$height, function($constraints){
                    return $constraints->aspectRatio();
                })->save($path."/".$file_name);
            }
            return $file_name;
        }else{
            return null;
        }
    }


    function deleteImage($image, $dir){
         $path = public_path().'/uploads/'.$dir;

         if($image != null && file_exists($path.'/'.$image)){
             unlink($path.'/'.$image);
         }

    }
