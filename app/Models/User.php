<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role',
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function userInfo()
    {
        return $this->hasOne("App\Models\UserInfo", 'user_id', 'id');
       // return $this->belongsTo("App\Models\UserInfo", 'user_id', 'id');
    }

    public function updateUserRules()
    {
        return [
            'name'  =>'required|string|max:50',
            'phone'  =>'nullable|string',
            'address'  =>'nullable|string',
            'image'  =>'sometimes|image|max:7000',
        ];
    }

    public function passwordUpdateRule(){
        return [
            'password'  => 'required|confirmed|min:8' //regular expression ni yehi garnu parxa
        ];
    }
}
