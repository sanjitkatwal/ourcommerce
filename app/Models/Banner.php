<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    use HasFactory;

    protected  $fillable = ['title', 'link', 'image', 'status', 'created_by'];

    public function bannerValidateRules($act = 'add'){
        return $rules = [
            'title'         => 'required|string|max:150',
            'link'          => 'required|url',
            //'image'         => ($act == 'add' ? 'required|' : 'sometimes|')."image|max:7000",
            'status'        => 'required|in:active,inactive',

        ];
    }
}
