<!-- START HEADER-->
<header class="header">
    <div class="page-brand">
        <a class="link" href="{{ route('landing') }}" target="_new">
            <span class="brand">Our
                <span class="brand-tip">Commerce</span>
            </span>
            <span class="brand-mini">OC</span>
        </a>
    </div>
    <div class="flexbox flex-1">
        <!-- START TOP-LEFT TOOLBAR-->
        <ul class="nav navbar-toolbar">
            <li>
                <a class="nav-link sidebar-toggler js-sidebar-toggler"><i class="ti-menu"></i></a>
            </li>
        </ul>
        <!-- END TOP-LEFT TOOLBAR-->
        <!-- START TOP-RIGHT TOOLBAR-->
        <ul class="nav navbar-toolbar">
            <li class="dropdown dropdown-user">
                <a class="nav-link dropdown-toggle link" data-toggle="dropdown">
                    @if(auth()->user()->userInfo!= null && auth()->user()->userInfo->image != null &&
                        file_exists(public_path().'/uploads/user/'.auth()->user()->userInfo->image))
                    <img src="{{ asset('uploads/user/'.auth()->user()->userInfo->image) }}" />
                    @else
                        <img src="{{ asset('images/user.png') }}" alt="">
                    @endif
                    <span></span>
                    {{ auth()->user()->name }}
                    <i class="fa fa-angle-down m-l-5"></i>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">

                    <a class="dropdown-item edit-profile" href="javascript:;" onclick=""><i class="fa fa-user"></i>Profile</a>

                    <a class="dropdown-item" href="javascript:;" data-target="#change-pwd" data-toggle="modal"><i class="fa fa-key"></i>Change Password</a>
                    <li class="dropdown-divider"></li>
                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                    document.getElementById('logoutform').submit();"><i class="fa fa-power-off"></i>Logout</a>
                    {{ Form::open(['url' => route('logout'), 'id' => 'logoutform']) }}
                    {{ Form::close() }}
                </ul>
            </li>
        </ul>
        <!-- END TOP-RIGHT TOOLBAR-->
    </div>
</header>
<!-- END HEADER-->

<---- Change Password Modal ---->
<div class="modal fade" id="change-pwd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editProfileLabel">Update User Profile</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {{ Form::open(['url'=>route('update-password', auth()->user()->id), 'files'=>true ]) }}
            @method('patch')
            <div class="modal-body">
                <div class="form-group row">
                    {{ Form::label('password', 'Password:', ['class' => 'col-sm-3']) }}
                    <div class="col-sm-9">
                        {{ Form::password('password', ['class' =>'form-control form-control-sm',
                        'required'=>true, 'id' => 'password', 'placeholder'=>'Enter Your New Password']) }}
                    </div>
                </div>

                <div class="form-group row">
                    {{ Form::label('password_confirmation', 'Confirm Password:', ['class' => 'col-sm-3']) }}
                    <div class="col-sm-9">
                        {{ Form::password('password_confirmation', ['class' =>'form-control form-control-sm',
                        'required'=>true, 'id' => 'password_confirmation', 'placeholder'=>'Re-Enter Your New Password']) }}
                        <span class="alert-danger" id="error_password"></span>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="reset" class="btn btn-danger" data-dismiss="modal">
                    <i class="fa fa-redo"></i>
                    Reset
                </button>
                <button type="submit" class="btn btn-success" id="submit">
                    <i class="fa fa-paper-plane"></i>
                    Save changes
                </button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>


<---- Update Profile Modal ---->
<div class="modal fade" id="editProfile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editProfileLabel">Update User Profile</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {{ Form::open(['url'=>route('update-profile', auth()->user()->id), 'files'=>true ]) }}
            @method('patch')
                <div class="modal-body">
                    <div class="form-group row">
                        {{ Form::label('name', 'Name:', ['class' => 'col-sm-3']) }}
                        <div class="col-sm-9">
                            {{ Form::text('name', auth()->user()->name, ['class' =>'form-control form-control-sm',
                            'required'=>true, 'id' => 'name', 'placeholder'=>'Enter Your Name']) }}
                        </div>
                    </div>

                    <div class="form-group row">
                        {{ Form::label('phone', 'Phone:', ['class' => 'col-sm-3']) }}
                        <div class="col-sm-9">
                            {{ Form::tel('phone', (auth()->user()->userInfo != null)?auth()->user()->userInfo->phone: null, ['class' =>'form-control form-control-sm',
                            'required'=>false, 'id' => 'phone', 'placeholder'=>'Enter Your Phone Number']) }}
                        </div>
                    </div>
                    <div class="form-group row">
                        {{ Form::label('address', 'Address:', ['class' => 'col-sm-3']) }}
                        <div class="col-sm-9">
                            {{ Form::text('address', (auth()->user()->userInfo != null)?auth()->user()->userInfo->address: null,
                                ['class' =>'form-control form-control-sm','required'=>true, 'id' => 'address', 'placeholder'=>
                                'Enter Your Address', 'style'=>'resize:none', 'rows'=> 5]) }}
                        </div>
                    </div>

                    <div class="form-group row">
                        {{ Form::label('address', 'Address:', ['class' => 'col-sm-3']) }}
                        <div class="col-sm-3">
                            {{ Form::file('image', ['class' =>'form-control form-control-sm',
                            'required'=>false, 'id' => 'image']) }}
                        </div>
                        <div class="col-sm-2">
                            @if(auth()->user()->userInfo!= null && auth()->user()->userInfo->image != null &&
                                 file_exists(public_path().'/uploads/user/'.auth()->user()->userInfo->image))
                                <img src="{{ asset('uploads/user/'.auth()->user()->userInfo->image) }}" class="img img-fluid img-circle" />
                            @else
                                <img src="{{ asset('images/user.png') }}" alt=""  class="img img-fluid img-circle">
                            @endif
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="reset" class="btn btn-danger" data-dismiss="modal">
                        <i class="fa fa-redo"></i>
                        Reset
                    </button>
                    <button type="submit" class="btn btn-success">
                          <i class="fa fa-paper-plane"></i>
                        Save changes
                    </button>
                </div>
            {{ Form::close() }}
        </div>
    </div>
</div>

