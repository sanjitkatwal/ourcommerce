@extends('layouts.admin')
@section('title','Banner Creare, Admin || Dashboard')
@section('main-content')

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title">Banner Form</div>
                    <div class="ibox-tools">
                        <a href="{{ route('banner.create') }}" class="btn  btn-sm btn-success">
                            <i class="fa fa-plus"></i> Create Banner
                        </a>
                    </div>
                </div>
                <div class="ibox-body">
                    {{ Form::open(['url' =>route('banner.store'), 'class' =>'form', 'files' => true]) }}
                        <div class="form-group row">
                            {{ Form::label('title', 'Title: ', ['class' => 'col-sm-12 col-md-3']) }}
                            <div class="col-sm-12 col-md-9">
                                {{ Form::text('title', '', ['class'=>'form-control form-control-sm', 'id'=>'title',
                                    'required'=>true, 'placeholder' =>'Enter banner title here.']) }}
                                @error('title')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                    <div class="form-group row">
                        {{ Form::label('link', 'URL: ', ['class' => 'col-sm-12 col-md-3']) }}
                        <div class="col-sm-12 col-md-9">
                            {{ Form::url('link', '', ['class'=>'form-control form-control-sm', 'id'=>'link',
                                'required'=>true, 'placeholder' =>'Enter banner link here.']) }}
                            @error('link')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        {{ Form::label('status', 'Status: ', ['class' => 'col-sm-12 col-md-3']) }}
                        <div class="col-sm-12 col-md-9">
                            {{ Form::select('status', ['active' =>'Published', 'inactive'=> 'Un-published'],
                                ['class'=>'form-control form-control-sm', 'id'=>'status', 'required'=>true]) }}
                            @error('status')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        {{ Form::label('image', 'Status: ', ['class' => 'col-sm-12 col-md-3']) }}
                        <div class="col-sm-12 col-md-9">
                            {{ Form::file('image',['id'=>'image', 'accept'=>'image/*']) }}
                            @error('image')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="offset-md-3 col-sm-12 col-md-9">
                            {{ Form::button("<i class='fa fa-trash'></i> Reset", ['class' => 'btn btn-sm btn-danger', 'type'=>'reset']) }}
                            {{ Form::button("<i class='fa fa-paper-plane'></i> Submit", ['class' => 'btn btn-sm btn-success', 'type'=>'submit']) }}
                        </div>
                    </div>


                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>

@endsection
