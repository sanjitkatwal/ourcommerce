@extends('layouts.admin')
@section('title','Banner Page, Admin || Dashboard')
@section('main-content')

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title">Banner List</div>
                    <div class="ibox-tools">
                        <a href="{{ route('banner.create') }}" class="btn  btn-sm btn-success">
                            <i class="fa fa-plus"></i> Create Banner
                        </a>
                    </div>
                </div>
                <div class="ibox-body">
                    <table class="table table-striped table-hover">
                        <thead>
                            <th>Title</th>
                            <th>Link</th>
                            <th>Image</th>
                            <th>Status</th>
                            <th>Created By</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
