@include('admin.partials.top-header')

<div class="page-wrapper">
    @include('admin.partials.top-menu')
    @include('admin.partials.side-bar')

    <div class="content-wrapper">
        <!-- START PAGE CONTENT-->
        <div class="page-content fade-in-up">

            @include('admin.partials.notify')
            @yield('main-content')
        </div>
        <!-- END PAGE CONTENT-->
       @include('admin.partials.copy')
    </div>
</div>
<!-- BEGIN THEME CONFIG PANEL-->
@include('admin.partials.scripts')
