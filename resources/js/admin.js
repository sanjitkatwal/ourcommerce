global.$=global.jQuery=require("jquery");
require("bootstrap/dist/js/bootstrap");
require('metismenu/dist/metisMenu');
require('jquery-slimscroll/jquery.slimscroll');
require('../template/admin/assets/js/app')

$(document).on('click', '.edit-profile', function(e){
    e.preventDefault();
    $('#editProfile').modal('show');
})


$(document).on('keyup', '#password_confirmation', function(e){
    let pass = $('#password').val();
    let re_pass = $(this).val();
    if(pass != re_pass){
        $('#error_password').html('Password and re-typ password does not match.');
        $('#submit').attr('disabled', 'disabled');
    }else{
        $('#error_password').html('');
        $('#submit').removeAttr('disabled', 'disabled');
    }
    console.log(this);
})
